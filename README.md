# distances

A python package for calculating various distance measures.

# Installation

The project can be cloned locally using the command:

```shell
git clone https://gitlab.gwdg.de/c.fortmanngrote/distances.git
```

# Usage

Currently the following distance measures are implemented in the package:

* Euclidean distance

# Testing
The package is tested with pytest. Run

```shell
pytest test_euclidean.py
```

# License
The package is licensed under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## TODOs
- [ ] Add more distances
